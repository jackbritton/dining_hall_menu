#!/usr/bin/python

#note: all filepaths are relative! don't leave them that way

from bs4 import BeautifulSoup
from urllib.request import urlopen
from urllib.request import URLopener
import time
import subprocess

html = urlopen("https://umaine.edu/dining/menus/")
soup = BeautifulSoup(html, "html.parser")
fileManager = URLopener()

#numFiles is a counter and our naming iterator for this loop
#we're going through and getting the pdfs
numFiles = 0
for a in soup.findAll('a'):
    link = a.get("href")
    if "https://umaine.edu/dining/wp-content/uploads/sites/" in link:
        fileManager.retrieve(link, "menu" + str(numFiles) + ".pdf")
        numFiles = numFiles+1

#convert each pdf to a quality jpg
pixelDensity = 216
densityOption = "-density " + str(pixelDensity) + "x" + str(pixelDensity)
for i in range(0,numFiles):
    arg1 = "menu" + str(i) + ".pdf[0]"
    arg2 = "menu" + str(i) + ".jpg"
    subprocess.call("convert " + densityOption + " " + arg1 + " -flatten -resize 50% " + arg2, shell=True)

#clean up
subprocess.call("rm *.pdf", shell=True)
